Documentation for the SKA Signal Metric Generator
=================================================

This section will explain basic usage of the Metric Generator. This will explain how to do
basic local development, as well as the configs required to run the generator as part of the receive
workflow process.

This project is meant to be used along with the Signal Displays. Further reading can be found for
the Signal system `here <https://developer.skao.int/projects/ska-sdp-qa-data-api/en/latest/>`_.

The code can be found on `GitLab
<https://gitlab.com/ska-telescope/sdp/ska-sdp-qa-metric-generator>`_.


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   changelog
   development
   usage
   local_usage

