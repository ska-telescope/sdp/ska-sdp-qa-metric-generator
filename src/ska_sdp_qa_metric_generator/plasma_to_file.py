"""Class to convert data from Plasma to the Signal Metrics API.

This class is meant to be run with the Plasma receivers, and as such requires
that the plasma store is operational, as well as data is written to it in the
correct way.
"""

import logging
from json import dumps

import msgpack
import msgpack_numpy
from overrides import override
from ska_sdp_datamodels.visibility import Visibility

from ska_sdp_qa_metric_generator.metrics import metrics_to_dict, process_metrics
from ska_sdp_qa_metric_generator.plasma_common_processor import SignalDisplayCommon
from ska_sdp_qa_metric_generator.utils.config import get_metrics_list
from ska_sdp_qa_metric_generator.utils.flow import SignalDisplayFlow
from ska_sdp_qa_metric_generator.utils.utilities import Timer
from ska_sdp_qa_metric_generator.utils.visibility import VisibilityHelper

logger = logging.getLogger(__name__)
msgpack_numpy.patch()


# pylint: disable=duplicate-code


class SignalDisplayMetricsFile(SignalDisplayCommon):  # pragma: no cover
    """Processer to send data to the signal displays."""

    def __init__(
        self,
        metrics: list[str] = None,
    ):
        super().__init__(
            random_ids=False,
            ignore_config_db=True,
            disable_kafka=True,
            enable_stats=False,
        )

        self.index_id = 0

        self.metrics = get_metrics_list(metrics)

        self._flows = {
            metric: [
                SignalDisplayFlow(
                    name=f"{metric}",
                    topic="",
                    host="",
                    data_format="msgpack_numpy",
                    metric_type=metric,
                    nchan_avg=1,
                    flow=None,
                    state=None,
                    additional_windows=0,
                    rounding_sensitivity=5,
                )
            ]
            for metric in self.metrics
        }

    @staticmethod
    @override
    def create(argv) -> "SignalDisplayMetricsFile":
        """Create instance of SignalDisplayMetricsFile."""
        parser = SignalDisplayCommon.create_parser("SignalDisplayMetricsFile", "")
        parser.add_argument(
            "--metrics",
            type=str,
            help="Comma seperated list of metrics",
            default="all",
        )
        args = parser.parse_args(argv)

        return SignalDisplayMetricsFile(
            metrics=args.metrics.split(","),
        )

    async def _process_dataset(self, dataset: Visibility):
        local_dataset = VisibilityHelper(
            dataset,
            self._processing_block_id,
            self._subarray_id,
            self._execution_block_id,
        )

        send = process_metrics(self.metrics, self._flows, local_dataset)

        with Timer(logger, "Save all data"):
            for index, (flow, value) in enumerate(send):
                with open(
                    f"{flow.topic}_{self.index_id}_{index}.json", "w", encoding="utf8"
                ) as file:
                    file.write(dumps(metrics_to_dict(value)).decode())
                with open(f"{flow.topic}_{self.index_id}_{index}.msgpack", "wb") as file:
                    file.write(msgpack.packb(metrics_to_dict(value)))
        self.index_id += 1


def main():  # pragma: no cover
    """A do nothing main."""
    print("This file should not be run on it's own, an example to run this file would be:")
    print(
        "plasma-processor "
        "ska_sdp_qa_metric_generator.plasma_to_file.SignalDisplayMetricsFile "
        "--metrics all"
    )


if __name__ == "__main__":  # pragma: no cover
    main()
