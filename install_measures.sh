#!/bin/sh

# This file is copied from: https://gitlab.com/ska-telescope/sdp/ska-sdp-realtime-receive-processors/-/blob/main/install_measures.sh?ref_type=heads
set -e

if [ $# -lt 1 ]; then
    echo "Usage: $0 <casacore-measure-dir>" 1>&2
    exit 1
fi
target_dir="$1"

# Check if casa data has already been configured
if [ -f "$HOME/.casarc" ] && grep -q measures.directory "$HOME/.casarc"; then
    echo "~/.casarc already configured with measures.directory, using existing measures install"
    exit 0
fi

# Install data directory
if [ -d "$target_dir" ]; then
    echo "directory $target_dir already exists, configuring to existing directory"
else
    mkdir -p "$target_dir"
    cd "$target_dir"
    curl -L ftp://ftp.astron.nl/outgoing/Measures/WSRT_Measures.ztar | tar xz
fi

# Point casacore runtime to measures install
echo "measures.directory: $target_dir" | tee -a ~/.casarc
echo "configured ~/.casarc"
